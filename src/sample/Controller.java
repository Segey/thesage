package sample;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import sample.path.theOs;

public class Controller implements Initializable    {
    @FXML private Button m_cancel = null;   //  fx:id="m_cancel"
    @FXML private Button m_run = null;

    @FXML private CheckBox m_create_assemblies_file = null;
    @FXML private CheckBox m_create_folders_file = null;
    @FXML private CheckBox m_headers = null;
    @FXML private CheckBox m_pch = null;
    @FXML private CheckBox m_double_pragma_once = null;

    @FXML private CheckBox m_lang_remove_unfinished_tags = null;
    @FXML private CheckBox m_lang_en_add_translate = null;

    @FXML private CheckBox m_release_copy_assemblies = null;
    @FXML private CheckBox m_release_deploy = null;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources)
    {
        if(theOs.isWin()) {
            m_release_deploy.setDisable(false);
        }
    }
    @FXML
    private void onCancel() {
    }
    public static final <T extends Thread>
    void createTask(ExecutorService service, T task) throws InterruptedException {
        service.submit(task);
        task.start();
    }
    @FXML
    private void onRun() throws InterruptedException {
        ExecutorService executor = Executors.newCachedThreadPool();
        if (m_create_assemblies_file.isSelected()) executor.execute(new sample.Task.Update.CreateASSEMBLIESFile.Main());
        if (m_create_folders_file.isSelected()) executor.execute(new sample.Task.Update.CreateFOLDERFile.Main());
        if (m_headers.isSelected()) executor.execute(new sample.Task.Update.Header.Main());
        if (m_pch.isSelected())     executor.execute(new sample.Task.Update.PCH.Main());
        if (m_double_pragma_once.isSelected())  executor.execute(new sample.Task.Determinate.DoublePragmaOnce.Main());
        // Language
        if (m_lang_remove_unfinished_tags.isSelected()) executor.execute(new sample.Task.Language.RemoveUnfinishedTags.Main());
        if (m_lang_en_add_translate.isSelected()) executor.execute(new sample.Task.Language.AddTranslateTagToEn.Main());
        // assemblies
        if (m_release_copy_assemblies.isSelected()) executor.execute(new sample.Task.Release.CopyAssemblies.Main());
        if (m_release_deploy.isSelected()) executor.execute(new sample.Task.Release.Deploy.Main());

        try {
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(" ---");
    }
}
