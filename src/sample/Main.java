package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.exception.PathNotFoundException;
import sample.path.thePaths;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            if(Files.notExists(Paths.get(thePaths.perfect())) == true)
                throw new PathNotFoundException();

            Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
            primaryStage.setTitle("the Sage");
            primaryStage.setScene(new Scene(root, 500, 300));
            primaryStage.show();
        } catch(PathNotFoundException e) {
            System.out.printf(" - Path not found '%s'", e.name());
        }
    }
    public static void main(String... args) {
         launch(args);
    }
}
