package sample.Task.Determinate.DoublePragmaOnce;

import sample.path.thePaths;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by dix on 20.10.16.
 * @note Finds double '#pragma once' in one file
 **/
public class Main implements Runnable {
    @Override
    public void run() {
        try {
            Walker walker = new Walker();
            Files.walkFileTree(Paths.get(thePaths.perfect()), walker);
            System.out.println(" - Determinate: The Finding double #pragma once task has been done!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
