package sample.Task.Determinate.DoublePragmaOnce;

import java.io.*;
import java.nio.file.*;
import java.security.InvalidParameterException;
import java.text.MessageFormat;
import java.util.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by dix on 13.11.16.
**/
public class Walker extends SimpleFileVisitor<Path> {
    private static PathMatcher m_predir = null;
    private static PathMatcher m_prefile = null;

    static {
        m_predir = FileSystems.getDefault().getPathMatcher("glob:{*.git,Output}");
        m_prefile = FileSystems.getDefault().getPathMatcher("glob:*.h");
    }
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return m_predir.matches(dir.getFileName())
                ? FileVisitResult.SKIP_SUBTREE
                : FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(m_prefile.matches(file.getFileName()) == false)
            return FileVisitResult.CONTINUE;

        final long count = Files.readAllLines(file).stream().filter(str -> str.equals("#pragma once")).count();
        if(count > 1) System.out.println(" --- Found a duplicate '#pragma once' in the file " + file);
        return FileVisitResult.CONTINUE;
    }
}
