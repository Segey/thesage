package sample.Task.Language.AddTranslateTagToEn;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.nio.file.Path;
import java.nio.file.Paths;
import sample.xml.Xml;
import sample.path.thePaths;

/**
 * Created by dix on 12.12.16.
**/
public class Main implements Runnable {
    private void removeOldTranslation(Path path) {
        try {
            DOMParser parser = new DOMParser();
            parser.parse(path.toString());
            Document doc = parser.getDocument();

            NodeList translations = doc.getElementsByTagName("translation");
            for(int i = 0; i != translations.getLength(); ++i)
                translations.item(i).getParentNode().removeChild(translations.item(i));

            Xml.write(path, doc);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void removeUnfinishedTags(Path path) {
        removeOldTranslation(path);
        try {
            DOMParser parser = new DOMParser();
            parser.parse(path.toString());
            Document doc = parser.getDocument();

            NodeList list = doc.getElementsByTagName("message");
            if(list.getLength() < 1) throw new Exception("Cannot find any message tags");

            for(int i = 0; i != list.getLength(); ++i) {
                NodeList nodes = list.item(i).getChildNodes();

                label:
                for(int j = 0; j != nodes.getLength(); ++j) {
                    if(nodes.item(j).getLocalName() != null && nodes.item(j).getLocalName().equalsIgnoreCase("source")) {
                        Node node = doc.createElement("translation");
                        node.setTextContent(nodes.item(j).getTextContent());
                        list.item(i).appendChild(node);
                        break label;
                    }
                }
            }
            doc.normalizeDocument();

            Xml.write(path, doc);

            System.out.println(String.format(" - Language: The Remove unfinished tags from the '%s' file task has been done!!!"
                    , path.getFileName()));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    @Override
    public final void run()
    {
        removeUnfinishedTags(Paths.get(thePaths.resources(), "langs/en.ts"));
    }
}
