package sample.Task.Language.RemoveUnfinishedTags;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import sample.path.thePaths;
import sample.xml.Xml;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by dix on 27.11.16.
**/
public class Main implements Runnable {
    public void removeUnfinishedTags(Path path) {
        try {
            DOMParser parser = new DOMParser();
            parser.parse(path.toString());
            Document doc = parser.getDocument();

            NodeList list = doc.getElementsByTagName("translation");
            if(list.getLength() < 1) throw new Exception("Cannot find any context tags");
            for(int i = 0; i != list.getLength(); ++i) {
                if(list.item(i).getAttributes().getNamedItem("type") != null)
                    list.item(i).getAttributes().removeNamedItem("type");
            }

            Xml.write(path, doc);
            System.out.println(String.format(" - Language: The Remove unfinished tags from the '%s' file task has been done!!!"
                    , path.getFileName()));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    @Override
    public void run() {
        removeUnfinishedTags(Paths.get(thePaths.resources(), "langs/en.ts"));
        removeUnfinishedTags(Paths.get(thePaths.resources(), "langs/ru.ts"));
    }
}
