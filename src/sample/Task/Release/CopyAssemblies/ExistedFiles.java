package sample.Task.Release.CopyAssemblies;

import sample.path.thePaths;
import sample.release.Assembly;
import sample.release.Dll;
import sample.release.Exe;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dix on 05.02.17.
 */
public class ExistedFiles {
    public static List<Assembly> get() {
        List<Assembly> list = new ArrayList<>();
        try {
            Files.walk(Paths.get(thePaths.output()))
                    .filter(Files::isRegularFile)
                    .filter(sample.release.Assembly::isAssembly)
                    .forEach(p -> {
                        if(sample.release.Assembly.isDll(p))
                            list.add(Dll.fromPath(Paths.get(thePaths.output()), p));
                        else
                            list.add(Exe.fromPath(Paths.get(thePaths.output()), p));
                    });
        }
        catch(IOException e)
        {
            System.out.println(String.format("Invalid travercing on '%s' folder!!!", thePaths.output()));
        }
        return list;
    }
}
