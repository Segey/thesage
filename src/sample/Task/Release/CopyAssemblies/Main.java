package sample.Task.Release.CopyAssemblies;

import java.util.List;
import sample.release.Assemblies;
import sample.release.Assembly;

/**
 * Created by dix on 24.01.17.
**/
public class Main implements Runnable {
    @Override
    public void run()
    {
        List<Assembly> _1 = Assemblies.assemblies();
        List<Assembly> _2 = ExistedFiles.get();
        _2.removeAll(_1);

        if(_2.isEmpty()) {
            System.out.println(String.format(" - Release: Copy Assemblies to '%s' folder task has been done!!!"
                    , sample.path.thePaths.release()));
            return;
        }

        for(Assembly a: _2)
           System.out.println(String.format(" - Found unnessary assembly \"%s\"", a));
    }
}