package sample.Task.Release.Deploy;

import sample.path.thePaths;
import sample.path.theQt;
import sample.release.Assemblies;
import sample.release.Assembly;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dix on 11.02.17.
**/
public class Main implements Runnable {
    @Override
    public void run()
    {
        List<Assembly> _1 = Assemblies.assemblies();
        String str = theQt.deploy() + " --release "  + _1.parallelStream()
                .map(a -> a.newPath(thePaths.release()))
                .collect(Collectors.joining(" "));
        try {
            Runtime.getRuntime().exec(str);
            System.out.println(String.format(" - Release: Deploy into the '%s' folder task has been done!!!"
                    , sample.path.thePaths.release()));
        }
        catch (IOException e) {
            System.out.println(String.format(" - Cannot execute %s", str));
            System.out.println(e);
        }
    }
}
