package sample.Task.Update.CreateASSEMBLIESFile;

import sample.path.thePaths;
import sample.release.Assemblies;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dix on 15.02.17.
 */
public final class Main implements Runnable {
    @Override
    public void run()
    {
        List<String> list = Assemblies.assemblies().parallelStream().map(i -> i.newPath(""))
                .collect(Collectors.toList());
        final String file = thePaths.perfect() + "ASSEMBLIES";
        try {
            Files.write(Paths.get(file), list);
            System.out.println(String.format(" - Update: Create '%s' file task has been done!!!", file));
        } catch (Exception e) {
            System.out.println(" - Cannot create the ASSEMBLIES file [" + e.getStackTrace() + "]");
        }
    }
}
