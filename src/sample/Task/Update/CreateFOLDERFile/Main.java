package sample.Task.Update.CreateFOLDERFile;

import sample.path.thePaths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @brief Created by SPanin on 7/25/2016.
 * @note Creates the FOLDERS file that contains a list of projects files
**/
public class Main implements Runnable {
    @Override
    public void run() {
        try {
            Walker walker = new Walker();
            Files.walkFileTree(Paths.get(thePaths.perfect()), walker);
            walker.writeTo(thePaths.perfect() + "FOLDERS");
            System.out.println(" - Update: The FOLDER file has been updated!!!");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
