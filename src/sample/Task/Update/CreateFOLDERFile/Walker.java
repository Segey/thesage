package sample.Task.Update.CreateFOLDERFile;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by SPanin on 7/25/2016.
**/
public class Walker extends SimpleFileVisitor<Path> {
    private static PathMatcher m_predir = null;
    private Set<String> m_set = new TreeSet<>();

    static {
        m_predir = FileSystems.getDefault().getPathMatcher("glob:{*.git,Output}");
    }
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return m_predir.matches(dir.getFileName())
                ? FileVisitResult.SKIP_SUBTREE
                : FileVisitResult.CONTINUE;
    }
   @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        m_set.add(dir.getParent().toString());
        return FileVisitResult.CONTINUE;
    }
    public void writeTo(String file) {

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(file), StandardCharsets.UTF_8, StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
            for(String s: m_set) {
                final String name = "Perfect";
                final int pos = s.indexOf(name);
                final int pos2 = pos + name.length() + 1;
                if(pos > 0 && pos2 < s.length() - 1) {
                    s = s.substring(pos2);
                    s = s.replace('\\', '/');
                    writer.write(s + '\n');
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
