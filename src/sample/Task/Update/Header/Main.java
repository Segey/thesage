package sample.Task.Update.Header;

import sample.path.thePaths;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by dix on 20.10.16.
 * @note Creates the FOLDERS file that contains a list of projects files
**/
public class Main implements Runnable {
    @Override
    public void run() {
        try {

            Walker walker = new Walker();
            Files.walkFileTree(Paths.get(thePaths.perfect()), walker);
            System.out.println(" - Update: The Header task has been done!!!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
