package sample.Task.Update.Header;

import java.io.*;
import java.nio.file.*;
import java.security.InvalidParameterException;
import java.text.MessageFormat;
import java.util.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @brief Created by dix on 20.10.16.
**/
public final class Walker extends SimpleFileVisitor<Path> {

    private static PathMatcher m_predir = null;
    private static PathMatcher m_prefile = null;

    static {
        m_predir = FileSystems.getDefault().getPathMatcher("glob:{*.git,Output}");
        m_prefile = FileSystems.getDefault().getPathMatcher("glob:*.h");
    }
    public final static String fileStr(Path file) {
        String str = file.toAbsolutePath().toString();
        final int pos = str.lastIndexOf("projects/");
        if(pos == -1) throw new InvalidParameterException();
        return MessageFormat.format(" * \\file      {0}", str.substring(pos + "projects/".length()));
    }
    public final static String copyrightStr() {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis());
        return String.format(" * \\copyright S.Panin, 2006 - %s", cal.get(Calendar.YEAR));
    }
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return m_predir.matches(dir.getFileName())
                ? FileVisitResult.SKIP_SUBTREE
                : FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(m_prefile.matches(file.getFileName()) == false)
            return FileVisitResult.CONTINUE;

        try {
            List<String> strings = Files.readAllLines(file);
            strings.set(1, fileStr(file));
            strings.set(4, copyrightStr());
            Files.write(file, strings);
        } catch(Exception e) {
            System.out.println(" - Invalid file " + file + "[" + e.getStackTrace() + "]");
        }
        return FileVisitResult.CONTINUE;
    }
}
