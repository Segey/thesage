package sample.Task.Update.PCH;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by dix on 26.10.16.
**/
public class Creator {
    private List<Path> m_list = new ArrayList<>();

    public Creator(List<Path> list) {

        this.m_list = list;
    }
    public final void create() {
        ExecutorService executor = Executors.newCachedThreadPool();

        for(Path file: m_list)
            if(file.endsWith("main_pch.h") == false)
                executor.execute(new FolderRunner(file));

        try {
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
