package sample.Task.Update.PCH;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by dix on 27.10.16.
 */
public class FolderRunner implements Runnable {
    private Path m_file = null;
    public FolderRunner(Path file) {
        super();
        m_file = file;
    }
    @Override
    public void run() {
        PCHWalker walker = new PCHWalker();
        try {
            Files.walkFileTree(m_file.getParent(), walker);
            walker.updatePCHfile(m_file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
