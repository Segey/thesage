package sample.Task.Update.PCH;

import sample.path.thePaths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by dix on 24.10.16.
 */
public class Main implements Runnable {
    @Override
    public void run() {
        try {
            Walker walker = new Walker();
            Files.walkFileTree(Paths.get(thePaths.perfect()), walker);

            Creator creator = new Creator(walker.list());
            creator.create();
            System.out.println(" - Update: The PCH task has been done!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
