package sample.Task.Update.PCH;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created by dix on 26.10.16.
**/
public class PCHWalker extends SimpleFileVisitor<Path> {
    private static PathMatcher m_predir = null;
    private static PathMatcher m_prefile = null;
    private Set<String> m_qset = new TreeSet<>();
    private Set<String> m_set  = new TreeSet<>();

    static {
        m_predir = FileSystems.getDefault().getPathMatcher("glob:{*.git,Output}");
        m_prefile = FileSystems.getDefault().getPathMatcher("glob:*.{cpp,h}");
    }
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return m_predir.matches(dir.getFileName())
                ? FileVisitResult.SKIP_SUBTREE
                : FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(m_prefile.matches(file.getFileName())) {
            m_qset.addAll(Files.lines(file).filter(m -> m.indexOf("#include <Q") == 0).collect(Collectors.toList()));
            m_set.addAll(Files.lines(file).filter(m -> m.matches("^#include <[^Q].*[^(\\.h)]>$")).collect(Collectors.toList()));
        }

        return FileVisitResult.CONTINUE;
    }
    public final void updatePCHfile(Path pch) {
        try {
            List<String> list = Files.lines(pch).limit(10).collect(Collectors.toList());
            list.addAll(m_set.stream().sorted(Comparator.comparing(s -> s.length())).collect(Collectors.toList()));
            list.addAll(m_qset.stream().sorted(Comparator.comparing(s -> s.length())).collect(Collectors.toList()));
            Files.write(pch, list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
