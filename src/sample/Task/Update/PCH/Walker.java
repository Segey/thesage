package sample.Task.Update.PCH;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SPanin on 7/25/2016.
 **/
public class Walker extends SimpleFileVisitor<Path> {
    private static PathMatcher m_predir = null;
    private static PathMatcher m_prefile = null;
    private List<Path> m_list = new ArrayList<>();

    static {
        m_predir = FileSystems.getDefault().getPathMatcher("glob:{*.git,Output}");
        m_prefile = FileSystems.getDefault().getPathMatcher("glob:*_pch.h");
    }
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return m_predir.matches(dir.getFileName())
                ? FileVisitResult.SKIP_SUBTREE
                : FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if(m_prefile.matches(file.getFileName()))
            m_list.add(file);

        return FileVisitResult.CONTINUE;
    }
    public final List<Path> list() {
        return m_list;
    }
}
