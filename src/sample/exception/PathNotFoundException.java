package sample.exception;

import java.io.IOException;
import sample.path.thePaths;

public class PathNotFoundException extends IOException {
    private String m_path = thePaths.perfect();

    public PathNotFoundException() {
    }

    public PathNotFoundException(String var1) {
        m_path = var1;
    }
    public final String name() {
        return m_path;
    }
}
