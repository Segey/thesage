package sample.path;

/**
 * Created by SPanin on 7/25/2016.
 */
public final class theOs {
    private final static String OS = System.getProperty("os.name").toLowerCase();
    public final static boolean isWin() { return (OS.indexOf("win") >= 0); }
    public final static boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }
    public final static boolean isUnix() {
        return (OS.indexOf("nux") >= 0);
    }
}
