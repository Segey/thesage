package sample.path;

/**
 * Created by dix on 18.07.16.
**/
public final class thePaths {
    public static final String projects() {
        return theOs.isWin() ? "c:/projects/"
                : "/home/dix/projects/";
    }
    public static final String perfect() {
        return  projects() + "perfect/";
    }
    public static final String resources() {
        return  perfect() + "/projects/perfect/resources/";
    }
    public static final String bin() {
        return perfect() + "bin/";
    }
    public static final String other() {
        return perfect() + "other/";
    }
    public static final String debug() {
        return bin() + "debug/";
    }
    public static final String output() {
        return bin() + "release/";
    }
    public static final String release() {
        return other() + "release/";
    }
}
