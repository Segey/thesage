package sample.release;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dix on 18.07.16.
 **/
public class Assemblies {
    public static List<Assembly> simpleDialogs() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("aboutdlg"));
        list.add(new Dll("optionsdlg"));
        list.add(new Dll("regdlg"));
        list.add(new Dll("intdlg"));
        list.add(new Dll("pidlg"));
        return list;
    }

    public static List<Assembly> windows() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("notepad"));
        return list;
    }

    public static List<Assembly> panels() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("panels"));
        return list;
    }

    public static List<Assembly> dialogs() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("checkboxdlg"));
        list.add(new Dll("keyvaluedlg"));
        return list;
    }

    public static List<Assembly> gismos() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("giblock"));
        return list;
    }

    public static List<Assembly> calcs() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Exe("CalcsManager"));
        list.add(new Dll("wwcalc", "calcs"));
        list.add(new Dll("repcalc", "calcs"));
        list.add(new Dll("mxwcalc", "calcs"));
        list.add(new Dll("bmicalc", "calcs"));
        return list;
    }

    public static List<Assembly> records() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("repersonaldlg"));
        list.add(new Exe("RePersonalManager"));
        list.add(new Dll("resimpledlg"));
        list.add(new Exe("ReSimpleManager"));
        list.add(new Dll("msblooddlg"));
        list.add(new Exe("MsBloodManager"));
        list.add(new Dll("msbodydlg"));
        list.add(new Exe("MsBodyManager"));
        return list;
    }

    public static List<Assembly> reports() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("QtZint"));
        list.add(new Dll("limereport"));
        list.add(new Dll("resultrpt"));
        list.add(new Dll("diaryrpt"));
        return list;
    }

    public static List<Assembly> standarts() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Exe("Norms"));
        list.add(new Exe("MsBody"));
        return list;
    }

    public static List<Assembly> unit5() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Dll("u5exdlg"));
        list.add(new Dll("u5setdlg"));
        list.add(new Exe("Unit5"));
        list.add(new Exe("U5Workout"));
        return list;
    }

    public static List<Assembly> eg() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Exe("ExercisesGuide"));
        list.add(new Exe("egexercisedlg"));  // temp renames to dll
        list.add(new Dll("egleveldlg"));
        list.add(new Dll("egspeeddlg"));
        list.add(new Dll("egtypedlg"));
        list.add(new Dll("egsportdlg"));
        list.add(new Dll("egforcedlg"));
        list.add(new Dll("egagedlg"));
        list.add(new Dll("egadaptationdlg"));
        list.add(new Dll("egmechanicsdlg"));
        list.add(new Dll("egequipmentdlg"));
        list.add(new Dll("egmuscledlg"));
        return list;
    }
    public static List<Assembly> exes() {
        List<Assembly> list = new ArrayList<>();
        list.add(new Exe("Downloader"));
        list.add(new Exe("Foods"));
        list.add(new Exe("Ld"));
        list.add(new Exe("Updater"));
        list.add(new Exe("Box"));
        list.add(new Exe("thePerfect"));
        return list;
    }
    public static List<Assembly> assemblies() {
        List<Assembly> list = new ArrayList<>();

        list.addAll(simpleDialogs());
        list.addAll(windows());
        list.addAll(panels());
        list.addAll(dialogs());
        list.addAll(gismos());
        list.addAll(calcs());
        list.addAll(records());
        list.addAll(reports());
        list.addAll(standarts());
        list.addAll(unit5());
        list.addAll(eg());
        list.addAll(exes());
        return list;
    }
}

