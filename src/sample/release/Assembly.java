package sample.release;

import sample.path.theOs;

import java.nio.file.Path;

/**
 * Created by dix on 31.01.17.
 */
public abstract class Assembly
{
    private String m_name;
    private String m_offset;

    public Assembly(String name)
    {
        this(name, "");
    }
    public Assembly(String name, String offset)
    {
        m_name = name;
        m_offset = offset;
    }
    public void setName(String name) {
        m_name = name;
    }
    public String name() {
        return m_name;
    }
    public void setOffset(String offset) {
        m_offset = offset;
    }
    public String offset() {
        return m_offset;
    }
    public String newPath(String path) {
        if(m_offset.isEmpty()) return path + m_name;
        return path + m_offset + "/" + m_name;
    }
    public static boolean isAssembly(Path path) {
        String s = path.normalize().toAbsolutePath().toString();
        if(theOs.isWin())
            return (s.endsWith(".dll") || s.endsWith("exe"));
        return (s.endsWith(".so") || s.indexOf(".") == -1);
    }
    public static boolean isDll(Path path) {
        String s = path.normalize().toAbsolutePath().toString();
        return theOs.isWin() ? s.endsWith(".dll") : s.endsWith(".so");
    }
    @Override
    public String toString() {
        return "Name = " + m_name + (m_offset.isEmpty() ? "" : (" Offset = " + m_offset));
    }

    @Override
    public boolean equals(Object o) {
        boolean rv = false;
        if(o instanceof Assembly) {
            Assembly c2 = (Assembly)o;
            rv = c2.m_name.equals(this.m_name)
                    && c2.m_offset.equals(this.m_offset);
        }
        return rv;
    }
}
