package sample.release;

import sample.path.theOs;

import java.nio.file.Path;

/**
 * Created by dix on 04.02.17.
 */
public class Dll extends Assembly {
    public Dll(String name) {
        super(name);
    }
    public Dll(String name, String offset) {
        super(name, offset);
    }
    @Override
    public String newPath(String path) {
        return (super.offset().isEmpty())
                ? path + (theOs.isWin() ? "" : "lib") + super.name() + (theOs.isWin() ? ".dll" : ".so")
                : path + super.offset() + "/" + (theOs.isWin() ? "" : "lib") + super.name() + (theOs.isWin() ? ".dll" : ".so");
    }
    public static Dll fromPath(Path parent, Path path) {
        String name = path.getFileName().toString();
        name = theOs.isUnix() ? name.substring(3, name.length() - 3)
                              : name.substring(0, name.length() - 4);

        if(path.getParent().equals(parent))
            return new Dll(name);
        return new Dll(name, path.getParent().getFileName().toString());
    }
}
