package sample.release;

import sample.path.theOs;

import java.nio.file.Path;

/**
 * Created by dix on 04.02.17.
 */
public class Exe extends Assembly {
    public Exe(String name) {
        super(name);
    }

    public Exe(String name, String offset) {
        super(name, offset);
    }

    @Override
    public String newPath(String path) {
        return (super.offset().isEmpty())
                ? path + super.name() + (theOs.isWin() ? ".exe" : "")
                : path + super.offset() + "/" + super.name() + (theOs.isWin() ? ".exe" : "");
    }
    public static Exe fromPath(Path parent, Path path) {
        String name = path.getFileName().toString();
        if(theOs.isWin())
            name.substring(0, name.length() - 4);

        if(path.getParent().equals(parent))
            return new Exe(name);
        return new Exe(name, path.getParent().getFileName().toString());
    }

}
