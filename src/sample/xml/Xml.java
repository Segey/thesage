package sample.xml;

import org.w3c.dom.Document;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.nio.file.Path;

/**
 * Created by dix on 24.01.17.
 **/
public class Xml {
    static public void write(Path path, Document doc) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(path.toString()));
            transformer.transform(source, result);
        } catch (Exception e) {
            System.out.println(String.format("file - %s [%s]", path.toString(), e.toString()));
        }
    }
}
