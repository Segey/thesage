#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
# @file      stats.py
# @brief     The main file creates statistics from current project
# @author    S.Panin <dix75@mail.ru>
# @copyright S.Panin, 2016 - 2016
# @version   v.1.1
# @created   November (the) 01(th), 2016, 22:00 MSK
# @updated   November (the) 01(th), 2016, 22:00 MSK
# @TODO
#
import os
import time
import re

out_folder = "stats"   # folder to output file
m_lines = 0            # all lines in the project
all_symbols = 0        # all symbols in the project
all_files = 0          # all files in the project
all_files_java = 0     # all files *.h in the project
all_files_tests = 0    # all files with tests in the project
m_tests = 0            # max tests in the project


for root, dirs, files in os.walk("."):
    for name in files:
        if(None != re.search(r".*(Debug|Release|Ld|\.git).*$", root)):
            continue
        file = os.path.join(root, name)
        if(re.search(r"^(?!.*\\build\\).*\.java$", file) is None):
            continue
        all_files += 1
        if(re.search(r"^.*\.java$", file)):
            all_files_java += 1
        if(re.search(r"^.*_test\.h$", file)):
            all_files_tests += 1

        with open(file) as f:
            for line in f:
                m_lines += 1
                all_symbols += len(line)
                if re.search(r".*TPS_(VERIFY|ASSERT|COMPARE).*", line) is not None:
                    m_tests += 1

str = ("{0:-^42}\n".format(""))
str += "Current Day is   {0}\n".format(time.ctime())
str += "{0:<32}-{1:>8}\n".format("Max header files in the Project", all_files_java)
str += "{0:<32}-{1:>8}\n".format("Max tests files in the Project", all_files_tests - 1)
str += "{0:<32}-{1:>8}\n".format("Max files in the Project", all_files)
str += "{0:<32}-{1:>8}\n".format("Max tests in the Project", m_tests)
str += "{0:<32}-{1:>8}\n".format("Max lines in the Project", m_lines)
str += "{0:<32}-{1:>8}\n".format("Max symbols in the Project", all_symbols)
print (str)

tm = time.localtime()
file_out = "{0}/{1}_{2}.st".format(out_folder, tm.tm_mon, tm.tm_year)
print(file_out)

with open(file_out, "a+") as f:
    f.write(str)
